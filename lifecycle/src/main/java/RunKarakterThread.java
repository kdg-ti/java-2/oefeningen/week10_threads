/**
 * Mark Goovaerts
 * 2/12/2020
 */
public class RunKarakterThread {
    public static void main(String[] args) {
        int[] counter = {0}; // Trucje om een int schijnbaar effectivly final te maken
        Runnable myRunnable = () -> {
            while (true) {
                System.out.print((char) ('a' + counter[0]++) + " ");
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    break; // spring uit de loop
                }
            }
            System.out.println("Thread beëindigd");
        };
        //TODO vul hier aan
    }
}